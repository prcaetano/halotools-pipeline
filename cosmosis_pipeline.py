from __future__ import print_function
import sys
import numpy as np
from halotools_utils import simulate_xi
from cosmosis.gaussian_likelihood import GaussianLikelihood
from cosmosis.names import likelihoods, data_vector
from scipy.stats import norm
from scipy.special import erf


def multiplicative_correction(r, transition_scale, std_gaussian, amp_gaussian,
                              transition_width, one_halo_bias_amp):
    """
    Simulates multiplicative bias composed by a erf like bias on 1-halo plus a gaussian error on transition scale.

    r: np.ndarray
        positions to calculate multiplicative correction on.
    transition_scale: float
        scale of 1 to 2 halo transition (peak of gaussian component and center of erf component).
    std_gaussian: float
        standard deviation of gaussian modelling transition bias.
    amp_gaussian: float
        amplitude of gaussian modelling transition bias.
    transition_width: float
        width of erf component modelling 1 halo bias.
    one_halo_bias_amp: float
        amplitude of erf component modelling 1 halo bias.

    returns:
        multiplicative_correction: np.ndarray with same shape as r
            multiplicative correction
    """
    deviation = norm.pdf(np.log10(r),
                         loc=np.log10(transition_scale), scale=np.log10(1 + std_gaussian/transition_scale))
    deviation = amp_gaussian * deviation / deviation.max()
    deviation += one_halo_bias_amp/2. * (-erf((np.log10(r) - np.log10(transition_scale)) / transition_width) + 1)
    return 1. / (1. + deviation)



like_name = "galaxy_xi_hod"
sim_parameters = "simulation_parameters"
data_vector_parameters = "data_vector"
hod_parameters = "hod_parameters"
modelling_bias = "hod_modelling_bias_simulation"


class HODLikelihood(GaussianLikelihood):

    like_name = like_name

    def __init__(self, options):
        block = options.block
        self.sim = block.get_string(sim_parameters, "simulation_name", default="bolshoi")
        self.redshift = block.get_double(sim_parameters, "redshift", default=0.)
        self.halo_finder = block.get_string(sim_parameters, "halo_finder", default="rockstar")
        self.fname = block.get_string(sim_parameters, "halo_catalog_fname", default="")
        if self.fname == "":
            self.fname = None
        self.mdef = block.get_string(sim_parameters, "mdef", default="vir")
        self.conc_mass_model = block.get_string(sim_parameters, "conc_mass_model", default="direct_from_halo_catalog")
        self.n_realizations = block.get_int(sim_parameters, "n_realizations", default=1)

        self.xi_file = block[data_vector_parameters, "xi_file"]
        self.cov_file = block.get_string(data_vector_parameters, "xi_cov_file", default="")
        if self.cov_file == "":
            print("WARNING: No data covariance matrix provided. Using diagonal covariance with errors loaded from measurements.", file=sys.stderr)
            self.cov_file = None
        self.r, self.xi, self.xi_err = np.loadtxt(self.xi_file, unpack=True)

        self.transition_scale = block.get_double(modelling_bias, "transition_scale", default=1.25)
        self.std_gaussian = block.get_double(modelling_bias, "std_gaussian", default=0.9)
        self.amp_gaussian = block.get_double(modelling_bias, "amp_gaussian", default=0.)
        self.transition_width = block.get_double(modelling_bias, "transition_width", default=0.2)
        self.one_halo_bias_amp = block.get_double(modelling_bias, "one_halo_bias_amp", default=0.)

        self.hodpars = {
                        "sigmam": 0.43,
                        "massmin": 11.82,
                        "alpha": 1.15,
                        "mass0": 11.02,
                        "mass1": 13.01,
                        "fcentral": 0.5,
                       }

        self.simulated_xi, self.model = simulate_xi(self.hodpars, self.r, self.xi,
                                                    sim=self.sim,
                                                    sim_redshift=self.redshift,
                                                    halo_finder=self.halo_finder,
                                                    fname=self.fname, mdef=self.mdef,
                                                    conc_mass_model=self.conc_mass_model,
                                                    n_realizations=self.n_realizations)

        self.constant_covariance = False

        super(HODLikelihood, self).__init__(options)


    def build_data(self):
        return self.r, self.xi


    def build_covariance(self):
        try:
            return self.data_cov
        except AttributeError:
            if self.cov_file is not None:
                self.data_cov = np.loadtxt(self.cov_file)
            else:
                self.data_cov = np.diag(self.xi_err**2)
            return self.data_cov


    def build_inverse_covariance(self):
        if self.cov_file is None:
            self.inv_cov = np.diag(1./self.xi_err**2)
        else:
            self.inv_cov = super(HODLikelihood, self).build_inverse_covariance()
        return self.inv_cov


    def extract_covariance(self, block):
        simulated_xi = self.simulate_xi(block)
        self.theory_cov = np.cov(simulated_xi.T)
        self.data_cov = self.build_covariance()
        return self.data_cov + self.theory_cov


    def extract_theory_points(self, block):
        xi_mean = np.mean(self.simulate_xi(block), axis=0)

        correction = multiplicative_correction(self.r,
                                               self.transition_scale,
                                               self.std_gaussian, self.amp_gaussian,
                                               self.transition_width, self.one_halo_bias_amp)

        return xi_mean * correction


    @staticmethod
    def load_hod_dict(block):
        """ Loads hod parameters as dictionary from datablock. """
        hod_keys = ["sigmam", "massmin", "alpha", "mass0", "mass1", "fcentral"]
        hodpars = {key: block[hod_parameters, key] for key in hod_keys}
        return hodpars


    def simulate_xi(self, block):
        if self.hodpars != self.load_hod_dict(block):
            self.hodpars = self.load_hod_dict(block)
            self.simulated_xi, _ = simulate_xi(self.hodpars, self.r, self.xi,
                                               sim=self.sim, sim_redshift=self.redshift,
                                               halo_finder=self.halo_finder, fname=self.fname,
                                               mdef=self.mdef, conc_mass_model=self.conc_mass_model,
                                               model=self.model, n_realizations=self.n_realizations)
        return self.simulated_xi




setup, execute, cleanup = HODLikelihood.build_module()

